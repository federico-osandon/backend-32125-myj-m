// const  dotenv = require('dotenv').config()
// const process = require('process')


// _______________________ process object ________________________
// console.log(process)


// _______________________ process object ________________________

// console.log('Directorio actual de trabajo:', process.cwd()) 
// console.log('id del proceso: ',process.pid) 
// // process.exit()
// // console.log(process.ppid) 
// console.log('version de node: ',process.version)
// console.log('titulo del proceso: ',process.title)
// console.log('Sistema Operativo: ',process.platform)
// console.log('Uso de la memoria: ',process.memoryUsage())

//_______________________________________ exit ______________________________________________________/

// setInterval(() => {
//         console.log('Hola')
//     }, 2000)
    
    
//     setTimeout(() => {
//         process.exit(3)
//     }, 5000)
        
//_______________________________________ on ______________________________________________________/
    

    // process.on('beforeExit', (code) => {
    //     // process.exit()
    //     console.log(' proceso antes de salir del evento con código' , code) // 1 orden aparente
            
    // })
    
    // process.on('exit', (code) => { // 2 orden aparente
    //     console.log('a punto de salir del evento con código', code)
    // })


    // console.log('This message is displayed first.') //3 orden aparente


///////////////////////////////// uncaughtException ///////////////////////////

// process.on('uncaughtException', (err, origin) => {
//     console.error('We forgot to catch an error', err)
// })

// setTimeout(() => {
//     console.log('This will still run.')
// }, 1000)

// // Se fuerza una excepción pero no se recoge
// nonexistentFunc()// no extiste la función 
// console.log('Esto no se ejecutará')


/////////////////////// process execPath ///////////////////////

// console.log(process.execPath) // /usr/local/bin/node
// console.log(process.stdout) // /usr/local/bin/node

/////////////////////// process stdout write ///////////////////////
// process.stdout(1): el flujo de salida estándar, que es una fuente de salida del programa

// Un Stream de Escritura para stdout.
// Según la documentación de Node.js, stream es una interfaz para trabajar con una FLUJO de datos. Estos flujos pueden ser de entrada o de salida.



// console.log  = (message) => {
//     process.stdout.write(`${message} \n`)
// }
// console.log  = (message) => {
//     process.stdout.write(` - ${message} \n`)
// }
// console.log('hola')
// console.log('Fede')
// console.log('Cómo estás?')



// process.on("exit", (code) => {
//     console.log(`Saliendo con el código ${code}`);
//   })
  
//   const programa = (array) => {
//     const error = {};
  
//     if (!array) {
//       error.descripcion = "entrada vacía";
//       console.log({ error });
//       process.exit(-4);
//     }
  
//     const datos = {};
//     const numeros = [];
//     const tipos = [];
//     let suma = 0;
//     let counter = 0;
//     let max;
//     let min;
//     let check;
  
//     for (let j = 0; j < array.length; j++) {
//       const elem = array[j];
//       numeros.push(elem);
//         if (typeof elem != "number") {
//             check= false
//         }
//     //   typeof elem != "number" && (check = false);
//       tipos.push(typeof elem);
  
//       counter++;
//       suma += parseInt(elem);
  
//       (!max || max < elem) && (max = elem);
//       (!min || min > elem) && (min = elem);
//     }
  

//   if (!check) {
//       error.descripcion = "error de tipo";
//       error.numeros = numeros;
//       error.tipos = tipos;
  
//       console.log({ error });
//       process.exit(-5);
//     }
  
//     datos.numeros = numeros;
//     datos.promedio = suma / counter;
//     datos.max = max;
//     datos.min = min;
//     datos.ejecutable = process.execPath;
//     datos.pid = process.pid;
  
//     console.log({ datos });
//   };
  
//   //programa(process.argv);
// //   const test = [1, 2, 3, 4, 5, true, "hola", { key: "value" }];
//   const test = [1, 2, 3, 4, 5];
// //   programa(test);
//   programa(test);
  