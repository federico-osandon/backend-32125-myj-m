const { 
    exec, 
    execFile 
} = require('child_process')

//// Ejecutar todo con git bash ///////

/////////////////////////// exec ///////////////////////////

// exec('ls -lh', (err, stdout, stderr) => {
//     if (err) {
//         console.error(`exec error: ${err.message}`)
//         return
//     }
//     if (stderr) {
//         console.error(`stderr: ${stderr}`)
//         return        
//     }
    
//     console.log(`stdout: \n${stdout}`)
// })


//////////////////// execFile /////////////////////////// TEnfo que revisar

// execFile(__dirname + '/processNodejsImage.sh', (error, stdout, stderr) => {
//     console.log(__dirname+ '/processNodejsImage.sh')

//     if (error) {
//       console.error(`edecFile error: ${error.message}`);
//       return;
//     }
  
//     if (stderr) {
//       console.error(`stderr: ${stderr}`);
//       return;
//     }
  
//     console.log(`stdout:\n${stdout}`);
// })


/////////////////////// spawn ///////////////////////////

// const { spawn } = require('child_process')

// const child = spawn('find', ['.'])

// child.stdout.on('data', (data) => {
//     console.log(`stdout: ${data}`)
// })

// child.stderr.on('data', (data) => {
//     console.error(`stderr: ${data}`)
// })

// // child on error
// child.on('error', (error) => {
//     console.error(`child error: ${error.message}`)
// })

// // child on close
// child.on('close', (code) => {
//     console.log(`child process exited with code ${code}`)
// })


/////////////////////// fork ///////////////////////////

// const http = require('http') 


// const calculo = () => {
//     let suma = 0
//     for(let i=0; i < 6e9; i++) {
//         suma += i
//     }
//     return suma    
// }

// let visitas = 0

// const server = http.createServer()
// server.on('request', (req, res) => {
//     let { url } = req
//     if(url === '/calcular'){
//         const sum = calculo()
//         res.end(`La suma es ${sum}`)
//     }else if(url === '/'){        
//         res.end(`Ok ${++visitas}`)
//     }
// })

// const PORT = 8000

// server.listen(PORT, err => {
//     if(err) throw new Error(err)
//     console.log(`Server listening on port ${PORT}`)
// })


///////////////////////////////////////////////////77


const http = require('http') 
const { fork } = require('child_process')
// const calculo = require('./computo.js')


let visitas = 0

const server = http.createServer()

server.on('request', (req, res) => {
    let { url } = req
    if(url === '/calcular'){
        const computo = fork('./computo.js')
        // console.log(computo)
        computo.send('start')
        computo.on('message', mensaje => {
            console.log(mensaje)
            res.end(mensaje)
        })
        
        // res.end(`La suma es `)
    }else if(url === '/'){        
        res.end(`Ok ${++visitas}`)
    }
})

const PORT = 8000

server.listen(PORT, err => {
    if(err) throw new Error(err)
    console.log(`Server listening on port ${PORT}`)
})