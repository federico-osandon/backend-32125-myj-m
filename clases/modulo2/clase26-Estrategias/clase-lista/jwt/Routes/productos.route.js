const { Router } = require('express')
const { authMiddleware } = require('../middlewares/auth.middleware')
const { auth } = require('../middlewares/authJwt.middleware')
const Products = require('../services/productos.js')

const routerfProductos = Router()
const products = new Products()


routerfProductos.get('/', auth,(req, res) => {
    console.log(req.session.visits)
    // const productos = new Products
    const productos = products.getProducts()
    if(productos.error) res.status(200).json({msg: 'No hay productos cargados'}) 
    res.status(200).json(productos)
})

routerfProductos.get('/:id', (req, res) => {
    // const productos = new Products
    const producto = products.getProductById(req.params.id)
    if(producto.error) res.status(404).json({msg: 'Producto no encontrado'})
    res.status(200).json( producto )
})

routerfProductos.post('/', (req, res) => {
    // const productos = new Products
    console.log(req.body)
    const producto = products.addProduct(req.body)
    res.status(201).json(producto)
})

routerfProductos.put('/:id', (req, res) => {
    // const productos = new Products
    const producto = products.updateProduct(req.params.id, req.body)
    res.status(200).json(producto)
})

routerfProductos.delete('/:id', (req, res) => {
    // const productos = new Products
    const producto = products.deleteProduct(req.params.id)
    res.status(200).json(producto)
})

module.exports = routerfProductos