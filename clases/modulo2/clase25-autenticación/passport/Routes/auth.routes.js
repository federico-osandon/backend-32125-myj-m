const { Router } = require('express')
const passport = require('passport')
const { countVisits } = require('../middlewares/countVisits/countVisits.middleware')
const { users } = require('../usuarios/users')
// const { passport } = require('../middlewares/passport')

const authRouter = Router()


//____________________________________________ login _____________________________________ //
authRouter.get('/login', (req, res) => { // lleva la vista del formulario de login
    res.send('login')
})

authRouter.post('/login', passport.authenticate('login', {
    successRedirect: '/api/productos',
    failureRedirect: '/api/auth/login',
}))

//____________________________________________ register _____________________________________ //

authRouter.get('/register', (req, res) => {   // devuelve la vista de registro 
    res.send('register')
})

authRouter.post('/register', passport.authenticate('signup', {
    successRedirect: '/api/auth/login',
    failureRedirect: '/api/auth/register',
}))
//____________________________________________ logout _____________________________________ //

authRouter.get('/logout', (req, res) => { // cierra la sesion
    req.session.destroy(err =>{
        if(err) return res.send(err)
        res.send('<h1>Sesion cerrada Adeu</h1>')
    })
})


module.exports = { 
    authRouter 
}