
// tasklist /fi "imagename eq node.exe"
// taskkill /pid <PID> /f
// fuser <PORT>/tcp [-k]


// forever 
// forever start app.js
// forever list
// forever stop <PID>
// forever stopall

// pm2 modo fork 
//pm2 start app.js --name="Serverx" --watch -- PORT
//pm2 start app.js --name="Server1" --watch -- 8081
//pm2 start app.js --name="Server2" --watch -- 8082

//pm2 start app.js --name="Server3" --watch -i max -- 8083



const initServer = require("./server")

//nuevas
const cluster = require('cluster')
const http = require('http')
const numCPUs = require('os').cpus().length
// console.log(numCPUs)


const app = initServer()
// const port = process.env.PORT || 8000
const PORT = process.argv[2] || 8080

if (cluster.isPrimary) {
    console.log(`Master ${process.pid} is runing`)
    for(let i = 0; i<4; i++){
        cluster.fork()
    }
    cluster.on('exit', (worker, coder, signal)=>{
        console.log(`worker ${worker.process.pid} died`)
    })

} else {
    try {
        app.listen(PORT)
        console.log(`Escuchando en el puerto ${PORT}`)
        console.log(`Worker ${process.pid} started `);
    } catch (error) {
        console.log(error)
    }
    
}

