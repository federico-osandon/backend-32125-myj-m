const { Router } = require('express')
const { countVisits } = require('../middlewares/countVisits/countVisits.middleware')

const authRouter = Router()
const users = [ { id: 1, username: 'admin', password: 'admin', admin:true } ]

//____________________________________________ login _____________________________________ //
authRouter.get('/login', (req, res) => { // lleva la vista del formulario de login
    res.send('login')
})

authRouter.post('/login', countVisits ,async (req, res) => {
    try {
        console.log(req.session.visits)
        const { username, password } = req.body
        const user = users.find(u => u.username === username ) 
        
        if (!user) {
            return res.status(401).send('Usuario no existe')
        }

        if (user.password !== password) {
            return res.status(401).send('Usuario o contraseña incorrectos')
        }

        req.session.username = username
        req.session.admin = true
        
        res.send(`<h1>Bienvenido ${req.session.user}</h1> <h2>Visitas: ${req.session.visits}</h2>`)
    } catch (error) {
        res.status(500).send(error)
    }
    
})
//____________________________________________ register _____________________________________ //

authRouter.get('/register', (req, res) => {   // devuelve la vista de registro 
    res.send('register')
})

authRouter.post('/register', (req, res) => { // registra un usuario
    const { username, password, admin } = req.body
   
    const usuario = users.find(user => user.username === username )
    
    if (usuario) return res.status(400).json({ message: 'User already exists' })    

    users.push({ id: users.length + 1, username, password, admin })
    
    res.send('usuario registtrado')
})
//____________________________________________ logout _____________________________________ //

authRouter.get('/logout', (req, res) => { // cierra la sesion
    req.session.destroy(err =>{
        if(err) return res.send(err)
        res.send('<h1>Sesion cerrada Adeu</h1>')
    })
})


module.exports = { 
    authRouter 
}