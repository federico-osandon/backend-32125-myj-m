const express = require('express')
const compression = require('compression')

const app = express()

// app.use(compression())

const frase = ('Hola que tal').repeat(1000)

app.get('/', (req, res ) => {
    res.send('hello world')
})
app.get('/saludo', (req, res ) => {
    res.send(frase)
})
app.get('/saludozip', compression(),(req, res ) => {
    res.send(frase)
})

app.use('*', (req, res)=>{
    const { url, method} = req
    
    res.send(`No existe la ruta especificada: ${url} - con el método : ${method}`)
})

app.listen(4000,() => {
    console.log('escuchando en el puerto 4000')
})