const express = require('express')
const compression = require('compression')
const { logger } = require('./logger')

const app = express()

// app.use(compression())

const frase = ('Hola que tal').repeat(1000)

app.get('/', (req, res ) => {
    res.send('hello world')
})
// app.get('/saludo', (req, res ) => {
//     res.send(frase)
// })
// app.get('/saludozip', compression(),(req, res ) => {
//     res.send(frase)
// })
app.get('/suma', compression(),(req, res ) => {
    const {num1, num2} = req.query

    if(!num1){        
        logger.error('no está definido numero num 2')
        return  res.send('no está definido el numero uno')
    }
    
    if(!num2){        
        logger.warn('no está definido numero num 2')
        return  res.send('no está definido el numero dos')
    }
    logger.info(`La suma es: ${parseInt(num1) + parseInt(num2)}`)
    res.send(`${parseInt(num1) + parseInt(num2)}`)
})

app.use('*', (req, res)=>{
    const { url, method} = req
    logger.err('No existe la ruta')
    res.send(`No existe la ruta especificada: ${url} - con el método : ${method}`)
})

app.listen(4000, err => {
    if(err) logger.error('error en el servidor: ', err)
    console.log('escuchando en el puerto 4000')
})