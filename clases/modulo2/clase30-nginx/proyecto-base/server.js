// server express whit listen on port 8080

const express = require('express')
const logger = require('morgan')
const session = require('express-session')

require('dotenv').config()
const cors = require('cors')
const { Server: HttpServer } = require('http')
const { Server: IOServer } = require('socket.io')
// importacion de handlebars
const handlebars = require('express-handlebars')
const routerfProductos = require('./Routes/productos.route.js')
const initSocket = require('./utils/initSocket.js')
const { authRouter } = require('./Routes/auth.routes.js')

//_____________________________________________ mongo para session _____________________________________ //
const MongoStore = require('connect-mongo')
const advancedOptions = { useNewUrlParser: true, useUnifiedTopology: true }
//____________________________________________________________________________________________________ //
// const { passport } = require('../passport/middlewares/passport.js')


//____________________________________________________________________________________________


//____________________________________________________________________________________________


const initServer = () => {    

    const app = express()
    const httpServer = new HttpServer(app)
    const io = new IOServer(httpServer)

    /////////////////////// configuracion de handlebars /////////////////////////
    app.engine(
        "hbs",
        handlebars({
            extname: ".hbs",
            defaultLayout: 'index.hbs',
        })
    )
    app.set("view engine", "hbs")
    app.set("views", "./views")

    //////////////  middleware  ///////////////////////

    app.use(session({
        secret: 'secreto',
        cookie: {
            httpOnly: false,
            secure: false,
            maxAge: 1000 * 60 * 60 * 24
        },
        rolling: true,
        resave: true,
        saveUninitialized: false
    }))
    // app.use(session({
    //     secret: process.env.SECRET_KEY_SESSION,    
    //     store: MongoStore.create({
    //         mongoUrl: process.env.MONGOATLAS_URL,
    //         mongoOptions: advancedOptions,
    //     }),
    //     resave: true, 
    //     saveUninitialized: true
    // }))

    // app.use(passport.initialize())
    // app.use(passport.session())

    // app.use(express.static('public'))
    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))
    app.use(cors())
    app.use(logger('dev'))

        
    ////////////////////// Rutas //////////////////////////////    

    app.use('/api/productos', routerfProductos)
    app.use('/api/auth', authRouter)
    
    const PORT = process.argv[2] || 8000

    //uno con info cluster
    const pid = process.pid
    app.get('/datos', (req, res)=>{
        console.log(`puerto ${PORT}`)
        res.send(`Servidor express - PORT ${PORT} - PID : ${pid} - FyH: ${new Date().toLocaleString()}`)
    })
    

    ////////////////////////////////////////////////////////


    app.get('/', (req, res) => {
        res.send('Hello World!')
    })


//_____________________________________________ socket.io _____________________________________ //   
    initSocket(io)
//______________________________________________________________________________________________//



    return {
        listen: () => new Promise((res, rej)=>{
            const server = httpServer.listen(PORT, () => {
                res(server)
            })
            server.on('error', err => rej(err))
        })
    }
    


    // https://upload.wikimedia.org/wikipedia/commons/8/81/Camiseta-negra.jpg

    // https://m.media-amazon.com/images/I/51XS20NbJnL._AC_UL320_.jpg

}

module.exports = initServer