const path = require('path')

require('dotenv').config()
 
const dotenv = require('dotenv')


console.log(process.env.MODO)


dotenv.config({
    path: process.env.MODO === 'byn'  
        ? path.resolve(__dirname, 'byn.env')      
        : path.resolve(__dirname, 'colores.env')      
})

console.log(process.env.FONDO, process.env.FRENTE)


module.exports = {
    NODE_ENV: process.env.NODE_ENV   || 'development',
    HOST: process.env.HOST           || 'localhost', 
    PORT: process.env.PORT           || 8080
}