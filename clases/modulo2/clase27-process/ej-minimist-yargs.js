// let argumentos = process.argv
// // console.log(argumentos.slice(2)[0])

// argumentos.forEach((item,index) =>{
//     console.log(`${index} --> ${item}`)
// })


// ___________________________ minimist _____________________________________//

// const parseArgs = require('minimist')
// const yargs = require('yargs')

// const args = parseArgs( process.argv.slice(2) )

// console.log(args)

// console.log(parseArgs( ['1', '2', '3', '4']  ));
// { _: [ 1, 2, 3, 4 ] } -> resultado

// console.log(parseArgs( ['-a', '1', '-b', '2', '3', '4'] ));
// { _: [ 3, 4 ], a: 1, b: 2 }

// console.log(parseArgs(['--n1', '1123', '--n2', '2', '3', '4']))

// console.log(parseArgs(['-ala', '1', '-b', '2', '--colores', '--watch']));

// console.log(parseArgs(['-a', '1', '-b', '2', '-c', '-x']));

///// mas ejemplos
// const options = { default: { nombre: 'Pepe', port: '8080' }}

// console.log(parseArgs(['-a', '1', '-b', '2', 'un-valor-suelto', '--nombre', 'juanita'], options))

// const options = { alias: { a: 'campoA', b: 'campoB', } }

// console.log(parseArgs( ['-a', '1', '-b', '2'] , options))
// { _: [], a: 1, campoA: 1, b: 2, campoB: 2 }

//// _____________________________________________ yargs ____________________________________________////
// const yargs = require('yargs/yargs')(process.argv.slice(2))

// const args = yargs.default(option).argv

// const args = yargs.default({
//     nombre: 'Fede',
//     apellido: 'Osandon'
// }).argv


// const args = yargs.alias({
//     n: 'nombre',
//     a: 'apellido'
// }).argv

// console.log(args)






// console.log(args)



