const express = require('express')
const config = require('./config')

const app = express()


console.log(`NODE_ENV: ${config.NODE_ENV}`)

app.get('/', (req, res) => {
    res.send('hello world')
})

app.listen(config.PORT, config.HOST, ()=>{
    console.log(`escuchando el puerto ${config.PORT}`)
})