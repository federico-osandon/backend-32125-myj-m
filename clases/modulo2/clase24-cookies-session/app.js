const express  = require('express')
const cookieParser = require('cookie-parser')
const session = require('express-session')

// const FileStore = require('session-file-store')(session)

// const redis = require('redis')
// const client = redis.createClient()
// const RedisStore = require('connect-redis')(session)

const MongoStore = require('connect-mongo')
const advancedOptions = { useNewUrlParser: true, useUnifiedTopology: true }

const cookiesRoutes = require('./src/routes/cookies/cookies.routes')
const sessionRoutes = require('./src/routes/session/session.routes')
require('dotenv').config()


const app = express()
const PORT = process.env.PORT || 3000

// session con archivos
// const config = {
//     path: './src/sessions',
//     ttl: 60 * 3600 * 24 * 168 ,// 1 week
//     retries: 0,
// }

// redis
// const config = {
//     host: 'localhost',
//     port: 6379,
//     client: client,
//     ttl: 60 * 3600 * 24 * 168 ,// 1 week
// }

// mongo local
const config = {
    
}

app.use(session({
    secret: process.env.SECRET_KEY_SESSION,
    // _______________________________________________
    // store: MongoStore.create({
    //     mongoUrl: 'mongodb://localhost:27017/sessions',
    // }),
    store: MongoStore.create({
        mongoUrl: process.env.MONGOATLAS_URL,
        mongoOptions: advancedOptions,
    }),
    // _______________________________________________
    // // _______________________________________________
    // store: new FileStore(config),
    // // _______________________________________________
    resave: true, 
    saveUninitialized: true
}))
app.use(cookieParser(process.env.SECRET_KEY_COOKIE))
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(express.static('public'))

app.use('/cookies',cookiesRoutes)
app.use('/session', sessionRoutes)

module.exports = app