
const ejecutar = (nombre, unaFuncion) => {
    // acciones
    return unaFuncion(nombre)
}

ejecutar( 'Fede', (nombre) => console.log(nombre) )


const operacion = (numero1, numero2 ,callback) => {    
    return callback(numero1, numero2)
}

const suma = (num1, num2) => num1 + num2
const resta = (num1, num2) => num1 - num2
const multiplicacion = (num1, num2) => num1 * num2
const div = (num1, num2) => num1 / num2



console.log(operacion(2, 3, suma))
console.log(operacion(2, 3, resta))
console.log(operacion(2, 3, multiplicacion))
console.log(operacion(2, 0, div))
