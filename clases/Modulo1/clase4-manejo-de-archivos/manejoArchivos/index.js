const fs = require('fs')

// console.log(fs)

// funcionalidades de fs sincrónicas__________________________________________________________________________

/* Writing the string 'hola conteenido \n' to the file data.txt. */
fs.writeFileSync('./data.txt', 'hola conteenido \n', 'utf-8' )

/* Appending the string 'hola  agregado \n' to the file data.txt. */
fs.appendFileSync('./data.txt', 'hola  agregado \n', 'utf-8' )

/* Reading the data.txt file and printing it to the console. */
try {
    // data
    const data = fs.readFileSync('./data.txt', 'utf8')
    console.log(data)
} catch (error) {
    console.log(error)
}

/* Deleting the file data.txt. */
fs.unlinkSync('./data.txt')

// funcionalidades de fs asincrónicas con callbacks ________________________________________________________


/* *|CURSOR_MARCADOR|* */
fs.writeFile('./data.txt', 'creando conteenido y archivo \n', 'utf-8' ,(err) => {
    if(err){
        console.log(err)
    }else{
        console.log('archivo creado')
    }
})

/* Appending the string 'Agregado de contendio' to the file data.txt. */
fs.appendFile('data.txt', 'Agregado de contendio', 'utf-8', err => {
    if(err){
        console.log(err)   
    }else{
        console.log('contenido agregado')
    }
})


/* The above code is reading the data.txt file and printing it to the console. */
fs.readFile('./data.txt', 'utf8', (err, data)=>{
    if(err){
        console.log(err)
    }else{
        console.log(data)
    }
})

f/* Deleting the file data.txt. */
s.unlink('./data.txt', err => {
    if(err){
        console.log(err)
    }else{
        console.log('archivo eliminado')
    }
})

/* Creating a folder called acavadata. */
fs.mkdir('./acavadata', err => {
    if(err){
        console.log(err)
    }else{
        console.log('carpeta creada')
    }
})

// /* Reading the directory and returning the contents of the directory. */
fs.readdir('./acavadata', (err, contenidoArchivo)=>{
    if(err){
        console.log(err)
    }else{
        console.log(contenidoArchivo)
    }
})

// manejo de archivos con promesas _________________________________________________________________________________________




const leerArchivo = async () => { 

    /* Reading the file data.txt and printing the content of the file. */
    fs.promises.readFile('./data.txt', 'utf8')
    .then(contenidoArchivo => console.log(contenidoArchivo))
    .catch(err => console.log(err))

    /* Creating a file called data.txt and writing the string 'Creando contenido' to it. */
    try {
        await fs.promises.writeFile('./data.txt', 'Creando contenido', 'utf-8')
        console.log('archivo creado')
    } catch (error) {
        console.log(error)
    }

    /* Appending the string 'Agregando contenido' to the file data.txt. */
    try {
        await fs.promises.appendFile('./data.txt', 'Agregando contenido', 'utf-8')
        console.log('archivo agregado')
    } catch (error) {
        console.log(error)
    }



    /* The above code is reading a file and parsing the content of the file. */
    try {
        const contenidoArchivo = await fs.promises.readFile('./data.txt', 'utf-8') 
        console.log(JSON.parse(contenidoArchivo))   
    } catch (error) {
        console.log(error)
    }


    /* Renaming the file data.txt to nuevo-nombre-archivo.txt. */
    try {
        await fs.promises.rename('./data.txt', './nuevo-nombre-archivo.txt')
    } catch (error) {
        console.log(error)
    }


}

leerArchivo()


