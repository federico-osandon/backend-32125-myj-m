

import { graphqlHTTP } from 'express-graphql';
import { personaSchema } from '../schemas/persona.schema.js'
import { UsuariosApi } from '../api/UsuariosApi';



class /* A class that is exporting a function that is returning a function. */
GraphQLController{
    constructor(){
        this.api = new UsuariosApi()
        this.config = {
          schema: personaSchema ,
          rootValue: {
            getPersona: this.api.getPersona,
            getPersonas: this.api.getPersonas,
            createPersona: this.api.createPersona,
            updatePersona: this.api.updatePersona,
            deletePersona: this.api.deletePersona
          }
        }
        return graphqlHTTP(this.config)
    }

    
}

module.exports = { GraphQLController }