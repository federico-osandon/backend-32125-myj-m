// ACfe1778d3485a3998cf463d190869f5fe sid

// +12069443810
import twilio from 'twilio'
import dotenv from 'dotenv'
dotenv.config()


const accountSid = process.env.ACCOUNT_SID
const authToken = process.env.AUTH_TOKEN

const client = twilio(accountSid, authToken)

;(async () => {
    try {
        const message = await client.messages.create({
            body: 'Hola soy un SMS de Node.js',
            from: process.env.PHONE_ENVIA,
            to: process.env.PHONE_RECIBE
        })
        console.log(message)
    } catch (error) {
        console.log(error)
    }
    
})()


 

