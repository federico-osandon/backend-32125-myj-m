// R9PzHCRFbJJ54wAfFb
// adriel.stamm@ethereal.email

import { createTransport } from "nodemailer"

const TEST_MAIL = 'adriel.stamm@ethereal.email'


const transporter = createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: TEST_MAIL,
        pass: 'R9PzHCRFbJJ54wAfFb'
    }
})

const mailOptions = {
    from: 'Servidor de Node js',
    to: TEST_MAIL, 
    subject: 'Mail de prueba desde node js',
    html: '<h1 style="color: blue;">Contenido de Prueba....</h1>'

}

;(async () => {
    try {
        const info = await transporter.sendMail(mailOptions)
        console.log(info)
    } catch (error) {
        console.log(error)
    }
})()