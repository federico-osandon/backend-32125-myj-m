const ProductosDaoDB = require("../Dao/ProductosDaoDB");


class ProductosApi{
    constructor(){
        this.productosDaos = new ProductosDaoDB ();
    }

    async agregar(producto){
        // console.log('api', producto)
        try {            
            const productoAgregado = await this.productosDaos.add(producto)
            // console.log('producto agregado', productoAgregado)
            return productoAgregado
        } catch (error) {
            console.log(error)
        }
    }

    async buscar(id){
        let productos
        if(id){
            productos = await this.productosDaos.getById(id)
        }else{
            productos = await this.productosDaos.getAll()
        }            
        return productos
    }

    // async borrar(id){
    //     if (id) {
    //         await this.productosDaos.deleteById(id)
    //     } else {
    //         await this.productosDaos.deletAll()
    //     }
    // }

    // async reemplazar(id, producto){
    //     const productoReemplazado = await this.productosDaos.updateById(id, producto)
    //     return productoReemplazado
    // }

    // exit(){
    //     this.productosDaos.close()
    // }
}

module.exports = ProductosApi