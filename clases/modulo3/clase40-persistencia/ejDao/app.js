const minimist = require('minimist')
const ProductosApi = require('./Api/ProductosApi')

console.log('Instanciando la API de productos')

const productosApi = new ProductosApi()

async function ejecutarCmds(){
    const argv = minimist(process.argv.slice(2))
    const {cmd, id, nombre, precio, stock} = argv
    try {
        switch(cmd.toLowerCase()){
            case 'buscar':
                console.log(cmd)
                console.log(await productosApi.buscar(id))
                break
            case 'agregar':
                console.log(cmd.toLowerCase())
                const producto = {nombre, precio, stock}
                console.log('producto index',producto)
                let productoAgregado = await productosApi.agregar(producto)
                console.log('producto agregado', productoAgregado)                
                // console.log(`producto agregado, ${productoAgregado}`)                
                break
            case 'reemplazar':
                console.log(cmd)
                console.log( productosApi.reemplazar(id, {nombre, precio, stock}))
                break
            case 'borrar':
                console.log(cmd)
                console.log( productosApi.borrar(id))
                break
            default:
                console.log('Comando no reconocido', cmd)
        } 
    } catch (error) {
        console.log(error)        
    }

    // productosApi.exit()
}



ejecutarCmds()