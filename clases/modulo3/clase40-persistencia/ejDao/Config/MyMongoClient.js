const { default: mongoose } = require("mongoose");
const Config = require("./Config");
const DbClient = require("./DbClient");

class MyMongoClient extends DbClient {
  constructor() {
    super()
    this.connected = false
    this.client = mongoose
  }

    async connect() {
        try {
            await this.client.connect(Config.db.cnxStr+Config.db.name, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false,
                useCreateIndex: true
            })
            this.connected = true
            console.log('Conectado a MongoDB')
        } catch (error) {
            throw new CustomError(500, 'Error al conectar a MongoDB 1', error)
        }
    }

    async disconnect() {
        try {
            await this.client.connection.close()
            console.log('Desconectado de MongoDB')
            this.connected = false
        } catch (error) {
            throw new CustomError(500, 'Error al desconectar de MongoDB', error)
        }
    }
}

module.exports = MyMongoClient