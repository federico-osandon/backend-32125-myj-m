import Producto from "./Producto"

export default class ProductosRepo {

    constructor() {
        this.dao = getDao()
    }
 
    async getAll() {
        const dtos = await this.dao.getAll({})
        return dtos.map(dto => new Producto(dto))
    }
 
    add(prod) {
        const dto = new ProductoDto(prod)
        return this.dao.save(dto)
    }
 
    addMany(prods) {
        const dtos = prods.map(p => new ProductoDto(p))
        return this.dao.saveMany(dtos)
    }
}
 