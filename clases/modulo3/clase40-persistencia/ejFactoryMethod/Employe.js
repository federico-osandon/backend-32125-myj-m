class Employe {
    // constructor(type) {
    //     this.type = type
    // }

    speak() {
        // console.log('employee',this.type)
        console.log("I am a "+ this.type +" employee");
    }
    // agregar item
    // listar items
    // listar item
    // actualizaar item

}

//________________________________________ 3 Clases ____________________________________ 

class FullTimeEmploye extends Employe { // -> persistencia en firestore 

    constructor(data) {
        super(data);
        this.type = "full time";
    }

}

class PartTimeEmploye extends Employe { // -> persistencia en mysql

    constructor(data) {
        super(data);
        this.type = "part time";
    }

}

class ContractorEmploye extends Employe { // -> persistencia en mongo 

    constructor(data) {
        super(data);
        this.type = "contractor";
    }

}


//// _____________________________________________________________________________________________________

class MyEmployeeFactory {

    createEmploye(data) {
        // console.log(data)
        if (data.type === "fulltime") return new FullTimeEmploye(data.type)
        if (data.type === "parttime") return new PartTimeEmploye(data.type)
        if (data.type === "contractor") return new ContractorEmploye(data.type)        
    }

}

/// __________________________________________________________________________________

const factory = new MyEmployeeFactory()

let types = ["fulltime", "parttime", "contractor"]
let employes = []

// for (let i = 0; i < 10; i++) {
//     let type = types[Math.floor(Math.random(2) * 2)]
//     employes.push(factory.createEmploye({type}))
// }

let type = 'mysql' //  -> .env
factory.createEmploye(type)

employes.forEach(employe => employe.speak())