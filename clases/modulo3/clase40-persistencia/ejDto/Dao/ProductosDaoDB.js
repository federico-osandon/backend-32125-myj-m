const CustomError = require("../Error/CustomError");
// let productos = []
class ProductosDaoDB {
    constructor(){
        this.productos = [{id: 1, nombre: 'RADIO', precio: 20, stock: 99 }] // coneccion a la base de datos
    }
    async getAll(){  
        try {
            return await this.productos
        } catch (error) {
            throw new CustomError(500, 'ERROR metodo getAll no implementado')                   
        }      
    }

    async getById(id){
        try {
            return await this.productos.find(producto => producto.id == id)
        } catch (error) {
            throw new CustomError(500, 'ERROR metodo getById no implementado')            
        }
    }

    async add(producto){
        try {
            await this.productos.push({id: this.productos.length+1, ...producto})
            console.log(this.productos)
            return producto
        } catch (error) {
            throw new CustomError(500, 'ERROR metodo add no implementado')            
        }
    }

    // async deleteById(id){
    //     throw new CustomError(500, 'ERROR metodo deleteById no implementado')
    // }

    // async deletAll(options){
    //     throw new CustomError(500, 'ERROR metodo deletAll no implementado')
    // }

    // async updateById(id, producto){
    //     throw new CustomError(500, 'ERROR metodo updateById no implementado')
    // }
    // close(){
    //     throw new CustomError(500, 'ERROR metodo close no implementado')
    // }
}

module.exports = ProductosDaoDB