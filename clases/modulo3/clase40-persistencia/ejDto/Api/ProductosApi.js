const { Cotizador } = require("../Cotizador");
const ProductosDaoDB = require("../Dao/ProductosDaoDB");
const { ProductoDto } = require("../ProductoDto");


class ProductosApi{
    constructor(){
        this.productosDao = new ProductosDaoDB ()
        this.cotizador = new Cotizador()
    }

    async agregar(producto){
        // console.log('api', producto)
        try {            
            const productoAgregado = await this.productosDao.add(producto)
            // console.log('producto agregado', productoAgregado)
            return productoAgregado
        } catch (error) {
            console.log(error)
        }
    }

    async buscar(id){
        let productos
        if(id){
            productos = await this.productosDao.getById(id)
        }else{
            productos = await this.productosDao.getAll()
        }            
        return productos
    }

    // async borrar(id){
    //     if (id) {
    //         await this.productosDao.deleteById(id)
    //     } else {
    //         await this.productosDao.deletAll()
    //     }
    // }

    // async reemplazar(id, producto){
    //     const productoReemplazado = await this.productosDao.updateById(id, producto)
    //     return productoReemplazado
    // }

    async buscarConCotizacionEnDolares(id) {
        if (id) {
            const producto = await this.productosDao.getById(id);
            const cotizaciones = {
                precioDolar: this.cotizador
                    .getPrecioSegunMoneda(producto.precio, 'USD')
            }
            const productoDto = new ProductoDto(producto, cotizaciones)
            // console.log(productoDto)
            return productoDto
        } else {
            const productos = await this.productosDao.getAll();
            const productosDtos = productos.map(producto => {
                const cotizaciones = {
                    precioDolar: this.cotizador
                        .getPrecioSegunMoneda(producto.precio, 'USD')
                }
                const productoDto = new ProductoDto(producto, cotizaciones)
                return productoDto
            })
            return productosDtos;
        }
    }
    

    // exit(){
    //     this.productosDao.close()
    // }
}

module.exports = ProductosApi