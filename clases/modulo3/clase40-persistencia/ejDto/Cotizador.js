class Cotizador {
    static VALOR_DOLAR = 300
 
    getPrecioSegunMoneda(precio, moneda) {
        switch (moneda) {
            case 'USD':
                return precio * Cotizador.VALOR_DOLAR
            default:
                return precio
        }
    }
}
module.exports = { Cotizador }
 