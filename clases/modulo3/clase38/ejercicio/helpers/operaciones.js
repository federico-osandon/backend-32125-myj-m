
const sumar = (a, b) => a + b
const restar = (a, b) => a - b
const mult = (a, b) => a * b
const div = (a, b) => a / b

module.exports = { sumar, restar, mult, div }
