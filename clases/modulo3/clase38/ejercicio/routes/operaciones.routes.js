const { Router }  = require('express')
const { restaGet, sumaGet, multGet, divGet, listarGet } = require('../controllers/operaciones.controllers')

const operacionesRouter = Router()


operacionesRouter.get('/', (req, res) => {
    res.send('Hola operaciones')
})

operacionesRouter.get('/suma', sumaGet)

operacionesRouter.get('/resta', restaGet)

operacionesRouter.get('/mult', multGet)

operacionesRouter.get('/div', divGet)

operacionesRouter.get('/listar', listarGet)

module.exports = operacionesRouter