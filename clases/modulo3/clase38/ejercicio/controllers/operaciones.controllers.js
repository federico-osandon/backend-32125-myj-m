const { guardarOperacion } = require("../servicio/operaciones")

 
const arrayOperaciones = []

const sumaGet = async (req, res) => {
    let { a, b } = req.query 
    
    
    const objOperacion = await guardarOperacion( a, b, 'suma')
    


    res.send(`La suma de ${a} y ${b} = ${ objOperacion.resultado } se guardó en la base de datos`)
}

const restaGet = (req, res) => {
    let { a, b } = req.query
    // negocio
    let objOperacion = {
        tipo: 'resta',
        params: [Number(a), Number(b)],
        resultado: Number(a) - Number(b),
        fecha: new Date()
    }

    //persistencia
    arrayOperaciones.push(objOperacion)

    res.send(`La resta de ${a} y ${b} es ${objOperacion}`)
}

const multGet = (req, res) => {
    let { a, b } = req.query
    // negocio


    let objOperacion = {
        tipo: 'multiplicacion',
        params: [Number(a), Number(b)],
        resultado: Number(a) * Number(b),
        fecha: new Date()
    }

    //persistencia
    arrayOperaciones.push(objOperacion)

    res.send(`La multiplicación de ${a} y ${b} es ${objOperacion}`)
}

const divGet =  (req, res) => {
    let { a, b } = req.query
    // negocio
    let objOperacion = {
        tipo: 'resta',
        params: [Number(a), Number(b)],
        resultado: Number(a) / Number(b),
        fecha: new Date()
    }

    //persistencia
    arrayOperaciones.push(objOperacion)
    res.send(`La división de ${a} y ${b} es ${objOperacion}`)
}

const listarGet = (req, res) => {
    res.send(arrayOperaciones)
}

module.exports = { sumaGet, restaGet, multGet, divGet, listarGet }