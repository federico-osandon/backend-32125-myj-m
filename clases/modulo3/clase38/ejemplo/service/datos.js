const { leerDatos } = require("../parsistenciaDao/datos")

const obtenerDatos = async (id)=>{
    
    return await leerDatos()    
}



const crearDato = async (dato)=>{
    dato.fechaCreacion = Date.now()
    // dato.edad = fechaNaciemietno - fechaActual
    await guardarDato(dato)
    return dato
}

module.exports = {
    obtenerDatos,
    crearDato,
}