const { Router } = require('express')
const { getDatos, postDatos } = require('../controller/datos')
const routerDatos = new Router()

routerDatos.get('/', getDatos)
routerDatos.post('/', postDatos)


module.exports = { routerDatos }