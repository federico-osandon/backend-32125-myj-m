const datosArray = []

async function leerDatos(){    
    return datosArray
}
async function leerDato(id){
    return await datosArray.find(item => item.id === id)
}

async function guardarDato(nuevoDato){
    datosArray.push(nuevoDato)
    return nuevoDato
}


module.exports = {
    leerDatos,
    guardarDato,
    leerDato
}