const { obtenerDatos, crearDato } = require("../service/datos")


const getDatos = async (req, res) => {
    const datos = await obtenerDatos()
    res.json(datos)
}

const postDatos = async (req, res) => {
    const newDatos = req.body //nombre, apellido y fecha nacimento
    await crearDato(newDatos)
    res.status(200).json(newDatos)
}

module.exports = { getDatos, postDatos }