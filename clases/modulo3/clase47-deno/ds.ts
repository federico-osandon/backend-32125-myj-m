// const saludo = (name: string): string => {
//     return `Hola ${name}`
// }

// console.log(saludo('Fede'))
//_____________________________________________________________________________
//https://deno.land/std@0.168.0/datetime/mod.ts
// import { parse } from 'https://deno.land/std@0.168.0/datetime/mod.ts'
// const data = parse("04-12-2023", "dd-MM-yyyy")

// console.log(data)
//_____________________________________________________________________________
// objeto global 
// console.log(Deno.args) // process.args

// const encoder = new TextEncoder()

// const data = encoder.encode('Hello Coder')

// await Deno.writeFile('text.txt', data)

// colores _________________________________________________________________

import { red, green, bgYellow, bgBlack, bgWhite, bold, blue, bgBlue, yellow } from "https://deno.land/std@0.168.0/fmt/colors.ts"


console.log(bgBlue(bold(yellow('Hola Fede como estás'))))
console.log(bgYellow(bold(green('Hola Fede como estás'))))
console.log(bgBlue(bold(green('Hola Fede como estás'))))