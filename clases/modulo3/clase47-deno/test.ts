// import {
//     assertEquals,
//     assertStrictEquals
// } from 'https://deno.land/std@0.100.0/testing/asserts.ts'
import { assertEquals, assertStrictEquals } from "https://deno.land/std@0.168.0/testing/asserts.ts";


Deno.test("example", function name(): void {
    let objUno = {hello: 'world'}
    let objDos = {hello: 'world'}
    assertEquals('World','World');
    assertEquals(objDos, objUno);    
})

Deno.test('isStrictlyEquals', (): void => {
    const a = {}
    const b = a

    assertStrictEquals(a,b)
})
Deno.test('isNotStrictlyEquals', function name(): void {
    const a = {}
    const b = a

    assertStrictEquals(a,b)
})
