
import { useEffect, useState } from 'react'
import ProductList from '../ProductList/ProductList'

const ProductListPaget = () => {
    const [productos, setProductos] = useState([])

    useEffect(()=>{
        // acciones api
        fetch('http://localhost:4000/api/productos')
        .then(response => response.json())
        .then(respServer => setProductos(respServer))
    }, [])
  
  return (
    <div>
        <ProductList productos={productos} />
    </div>
  )
}

export default ProductListPaget