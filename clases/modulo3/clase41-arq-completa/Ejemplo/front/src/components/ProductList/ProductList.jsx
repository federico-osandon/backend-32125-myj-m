import ProductCard from "../ProductCard/ProductCard"


const ProductList = ({productos}) => {
  return (
    <div>
        {productos.map(prod => <ProductCard key={prod.id} prod={prod}  />) }
    </div>
  )
}

export default ProductList