

const ProductCard = ({ prod }) => {
  return (
    <div>
      <h2>{prod.name}</h2>
      <h4>{prod.category}</h4>
      <img src={prod.thumbnail} alt={'imagne'}  style={{width: '25%'}}/>
      <h4>{prod.price}</h4>
      <h4>{prod.stock}</h4>
      <h4>{prod.code}</h4>
      <button>Detalle</button>
    </div>
  )
}

export default ProductCard