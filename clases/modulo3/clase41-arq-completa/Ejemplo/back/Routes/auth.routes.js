const { Router } = require('express')
const { countVisits } = require('../middlewares/countVisits/countVisits.middleware')
const { users } = require('../users/users')
const { generateToken } = require('../utils/generateToken')

const authRouter = Router()


//____________________________________________ login _____________________________________ //
authRouter.get('/login', (req, res) => { // lleva la vista del formulario de login
    res.send('login')
})

authRouter.post('/login', countVisits ,async (req, res) => {
    try {
        const { username, password } = req.body
        console.log(users)
        const usuario = users.find(user => user.username == username && user.password == password)
        if (!usuario) {
            return res.json({ error: 'usuario no existe' });
        }
        const { password:pass, id, ...user } = usuario
        const access_token = generateToken(user)

        res.json({ access_token })
    } catch (error) {
        res.status(500).send(error)
    }
    
})
//____________________________________________ register _____________________________________ //

authRouter.get('/register', (req, res) => {   // devuelve la vista de registro 
    res.send('register')
})

authRouter.post('/register', (req, res) => { // registra un usuario
    const { username, password, admin, direccion } = req.body
    users
    const yaExiste = users.find(usuario => usuario.username == username)
    if (yaExiste) {
      return res.json({ error: 'ya existe ese usuario' });
    }
   
    const nuevoUsuario = { 
        username,
        password,
        admin,         
        direccion 
    }
   
    users.push(nuevoUsuario)
   
    const access_token = generateToken({
        username,        
        admin,         
        direccion 
    })
   
    res.json({ access_token })
})
//____________________________________________ logout _____________________________________ //

authRouter.get('/logout', (req, res) => { // cierra la sesion
    req.session.destroy(err =>{
        if(err) return res.send(err)
        res.send('<h1>Sesion cerrada Adeu</h1>')
    })
})


module.exports = { 
    authRouter 
}