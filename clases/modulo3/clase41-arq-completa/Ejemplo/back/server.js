// server express whit listen on port 8080

const express = require('express')
const logger = require('morgan')
const session = require('express-session')

require('dotenv').config()
//cors
const cors = require('cors')

const { Server: HttpServer } = require('http')
const { Server: IOServer } = require('socket.io')
// importacion de handlebars
const handlebars = require('express-handlebars')
const routerfProductos = require('./Routes/productos.route.js')
const initSocket = require('./utils/initSocket.js')
const { authRouter } = require('./Routes/auth.routes.js')

//_____________________________________________ mongo para session _____________________________________ //
const MongoStore = require('connect-mongo')
const { options } = require('./Routes/productos.route.js')
const advancedOptions = { useNewUrlParser: true, useUnifiedTopology: true }
//____________________________________________________________________________________________________ //


  
//____________________________________________________________________________________________________ //


const initServer = () => {    

    const app = express()
    const httpServer = new HttpServer(app)
    const io = new IOServer(httpServer)

    /////////////////////// configuracion de handlebars /////////////////////////
    app.engine(
        "hbs",
        handlebars({
            extname: ".hbs",
            defaultLayout: 'index.hbs',
        })
    )
    app.set("view engine", "hbs")
    app.set("views", "./views")

    //////////////  middleware  ///////////////////////

    app.use(session({
        secret: 'secreto',
        cookie: {
            httpOnly: false,
            secure: false,
            maxAge: 1000 * 60 * 60 * 24
        },
        rolling: true,
        resave: true,
        saveUninitialized: false
    }))

    // app.use(session({
    //     secret: process.env.SECRET_KEY_SESSION,    
    //     store: MongoStore.create({
    //         mongoUrl: process.env.MONGOATLAS_URL,
    //         mongoOptions: advancedOptions,
    //     }),
    //     resave: true, 
    //     saveUninitialized: true
    // }))

   // ___________________________   conf cors ________________________//
   
        // const corsOptions = {
        //     origin: 'http://127.0.0.1:5173',
        //     credentials: true, <- esta permite cookies y encabezados de autorización 
        //     optionsSuccessStatus: 200,
        //     method: ['GET', 'POST']

        // }



        var allowList = ['http://127.0.0.1:5173', 'http://localhost:5174']

        const corsOptionsFunction = (req, callback) => {
            let corsOptions
        
            let isDomainAllowed = allowList.indexOf(req.header('Origin')) !== -1
            
            let isExtensionAllowed = req.path.endsWith('.jpg')
            
            console.log(isExtensionAllowed)
        
            if (isDomainAllowed) {
                corsOptions = { origin: true }
            }else {
                corsOptions = { origin: false }
            }
            callback(null, corsOptions)
        }
   // ___________________________   conf cors ________________________//



   app.use(express.static('public'))
   app.use( cors() )
   app.use(express.json())
    app.use(express.urlencoded({ extended: true }))
    app.use(logger('dev'))

        
    ////////////////////// Rutas //////////////////////////////    

    app.use('/api/productos', routerfProductos)
    app.use('/api/auth', authRouter)
    

    ////////////////////////////////////////////////////////


    app.get('/', (req, res) => {
        res.send('Hello World!')
    })


//_____________________________________________ socket.io _____________________________________ //   
    initSocket(io)
//______________________________________________________________________________________________//



    return {
        listen: port => new Promise((res, rej)=>{
            const server = httpServer.listen(port, () => {
                res(server)
            })
            server.on('error', err => rej(err))
        })
    }
    


    // https://upload.wikimedia.org/wikipedia/commons/8/81/Camiseta-negra.jpg

    // https://m.media-amazon.com/images/I/51XS20NbJnL._AC_UL320_.jpg

}

module.exports = initServer