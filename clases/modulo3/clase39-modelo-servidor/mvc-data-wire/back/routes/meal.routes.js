const express = require('express')
const mealControllers = require('../controllers/meal.controllers')
const router = express.Router()


router.get('/menu', mealControllers.getMenuController)

module.exports = router
