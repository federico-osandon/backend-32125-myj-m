const mealModels = require("../models/meal.models")

const meals = [
    { _id: 1, name: "Hamburguesa", price: 100, type: "Comida" },
    { _id: 2, name: "Papas fritas", price: 50, type: "Comida" },
    { _id: 3, name: "Coca cola", price: 25, type: "Bebida" },
    { _id: 4, name: "Agua", price: 15, type: "Bebida" },
    { _id: 5, name: "Postre", price: 50, type: "Postre" }
]


module.exports = {
    getMenuController: (req, res, next) => {
        // const meals = mealModels.getMeals()
        
        res.status(200).json(meals)
    }
}