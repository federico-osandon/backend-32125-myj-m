const express = require('express')
const cors = require('cors')
const mealRouter = require('./routes/meal.routes')

const app = express();


app.set('views', './views')
app.set('view engine', 'pug')

app.use(cors())

app.get('/', (req, res) => {
    res.send('Hello World!');
})

app.use('/api/meal', mealRouter);

const port = process.env.PORT || 4000
app.listen(4000, () => {
    console.log(`Escuchando en el puerto ${port}`);
})