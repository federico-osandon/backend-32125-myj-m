// IIFE

// funcióon de tipo flecha anónima
;(() => {
    console.log('Saludo auto ivnvocado')
})()
// initDB
// class Server{
//     constructor(){
//         ;(() => {
//             console.log('Base de datos datos conectada')
//         })()
        
        
//     }
// }
    
// const server = new Server()
// initDB
// class Server{
//     constructor(){
//         ;(() => {
//             console.log('Base de datos datos conectada')
//         })()
            // initServer()

//     }

// initServer(){
        //             console.log('Base de datos datos conectada')

    // }
// }
    
// const server = new Server()


// ____ singleton________________________________________

// class Server{
//     constructor(){
//         ;(() => {
//             console.log('Base de datos datos conectada')
//         })()
        
        
//     }
// }

let instance = null

class SingleteClass {
    constructor(){
        this.value = Math.random(100)
    }

    printValue(){
        console.log(this.value)
    }

    static getInstance(){
        if (!instance) {
            instance = new SingleteClass()
        } 
        return instance
    }
}


    
const singleteClass = SingleteClass.getInstance()
const singleteClass1 = SingleteClass.getInstance()

singleteClass.printValue()
singleteClass1.printValue()
console.log(singleteClass === singleteClass1)

// Date()



