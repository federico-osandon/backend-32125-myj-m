const swaggerJsdoc = require('swagger-jsdoc')

const options = {
    definition:{
        openapi: "3.0.0",
        info: {
            title: "Express generator API con Swagger",
            description: " Un simple CRUD con Express y documantación de Swagger"
        }
    },
    apis:['./docs/**/*.yaml']
}

const swaggerSpecs = swaggerJsdoc(options)

module.exports = { swaggerSpecs }