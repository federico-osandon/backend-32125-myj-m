var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


require('dotenv').config()
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var prodcutsRouter = require('./routes/products');

var app = express();


// ________________________________________ swagger ________________________________________|

const swaggerUi = require('swagger-ui-express')
const { swaggerSpecs } = require('./swaggerSpecs');


// ________________________________________ swagger ________________________________________|

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter)
//productos 
app.use('/api/products', prodcutsRouter)

// ruta swagger ________________________________________

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpecs))

// ruta swagger ________________________________________

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
