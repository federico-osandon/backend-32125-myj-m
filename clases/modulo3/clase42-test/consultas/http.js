// const http = require('https')

// const options = {
//     hostname: 'jsonplaceholder.typicode.com',
//     port: 80,
//     path: '/posts',
//     method: 'GET'
// }

// const req = http.request(options, res =>{
//     console.log(`statusCode: ${res.statusCode}`)
//     console.log(res)
//     res.on('data', (data) =>{
//         process.stdout.write(data)
//     })
// })

// req.on('error', (err) =>{
//     console.log(err)
// })

// req.end()


// post request_______________________________________________________________________

const https = require('http')

const data = JSON.stringify({
    todo: 'Buy the milk'
})

const options = {
    hostname: 'whatever.com',
    port: 80,
    path: '/todos',
    method: 'POST', 
    headers: {
        'Content-Type': 'application/json',
        'Content-length': data.length
    }
}

const req = https.request(options, res => {
    console.log(`statusCode: ${res.statusCode}`)

    res.on('data', d => {
        process.stdout.write(d)
    })
})

req.on('error', error => {
    console.log(error)
})

req.write(data)
req.end()