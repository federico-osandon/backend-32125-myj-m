const express = require('express')

const app = express()
const port = process.env.PORT || 4000

app.use(express.static('public'))
app.use(express.json())

app.get('/', (req, res) => {
    
    res.send('Helo world')
})


const initServer = () => app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
})
.on('error', (err) => {
    console.log(err)
})

module.exports = { initServer }