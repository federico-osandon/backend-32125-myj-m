

// const ProductoSchema = require("../models/productos.models");
let instance = null
class ContenedorMongo {
    constructor(colection){
        this.Item = colection
        
    }/* A function that connects to the database. */
    
    initContenedorMongo = () => new ContenedorMongo()

    
    getAllItems = async () =>  {
        const items = await this.Item.find()      
        console.log('items',items)
        if (items.length === 0) {
            return []
        }            
        return items
    }

    getItemById = async (id) => {
        
        const item = await this.Item.findById({_id: id}) 
        // console.log('item',item)
        if (!item) {
            return new Error('No se encuentra el item')
        }      
        return item
    }

    createItem = async (item) => {  
    
        // aagregar validaciones 
        const itemDb = await this.Item.findOne({nombre: item.nombre})
    
        if (itemDb) {
            return new Error('Ya se encuentra creado el item.')
        }
    
        const newItem = new this.Item(item)
        newItem.save()
        return newItem      
    }

    updateItem = async (id, item) => {
        return await this.Item.findByAndUpdate(id, data, {new: true})
        
    }

    deleteItem = async (id) => {
        return await this.Item.findByAndDelete(id) 
    }

    deleteAllItem = async () => {
        return await this.Item.deleteMany()
    }


}

  


  
//   exports.updateItem = function(id, item, options, callback) {
//     Item.findByIdAndUpdate(id, item, options, callback);
//   };
  
//   exports.deleteItem = function(id, callback) {
//     Item.findByIdAndRemove(id, callback);
//   };

module.exports = { ContenedorMongo }