const mongoose = require('mongoose');

// Conecta a la base de datos

const dbConnection = async () => {
    try {
        mongoose.connect('mongodb://localhost:27017/mydatabase', {
            useNewUrlParser: true,
            useUnifiedTopology: true
            // useCreateIndex: true,
            // useFindAndModify: false
        })
        console.log(`Base de datos conectada`)
    } catch (error) {
        console.log(error);
        throw new Error('Error a la hora de iniciar la base de datos');
    }
}




module.exports = { dbConnection }