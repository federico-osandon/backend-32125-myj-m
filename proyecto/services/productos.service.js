const { ProductoDAOsMongo } = require("../DAOs/ProductosDAOs/ProductosDAOsMongo")

class ProductosService {
    constructor() {
        this.productoDAOs = new ProductoDAOsMongo() 
    }

    async getAllProductos() {
        try {
            return await this.productoDAOs.getAllItems()             
        } catch (error) {
            console.log(error)
        }
    }

    async getProductoById(id) {
        try {
            const producto = await this.productoDAOs.getItemById(id)
            // console.log('producto service',producto)
            return producto
             
        } catch (error) {
                console.log(error)
            
        }
    }

    async createProducto(producto){
        try {
            const newProducto = {
                ...producto,
                fechaCreacion: Date.now()
                // agregar usuario
            }
            const nuevoProducto = await this.productoDAOs.createItem(newProducto)
            return nuevoProducto
        } catch (error) {
            console.log(error)
        }
    }
}

module.exports = { ProductosService }

