const { Schema, model } = require('mongoose');

const ProductoSchema = Schema({
    name: {
        type: String,
        required: [true, 'El nombre es obligatorio'],
        unique: true
    },
    estado: {
        type: Boolean,
        default: true,
        // required: true
    },
    // usuario: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'Usuario',
    //     required: true
    // },
    price: {
        type: Number,
        default: 0,
        required: true
    },
    category: {
        // type: Schema.Types.ObjectId,
        type: String,
        // ref: 'Categoria',
        required: true
    },
    description: { type: String },
    disponible: { type: Boolean, defult: true },
    imgUrl: { type: String },
});


ProductoSchema.methods.toJSON = function() {
    const { __v, estado, ...data  } = this.toObject();
    return data;
}


module.exports = model( 'Producto', ProductoSchema );
