const { Router } = require('express')
const { ProductosController } = require('../controllers/productos.controller')
const { authMiddleware } = require('../middlewares/auth.middleware')


// const Products = require('../DAOs/productosDAOs.js')
// const products = new Products()

const routerfProductos = Router()
const productoController = new ProductosController()
// console.log(productoController)


routerfProductos.get('/' ,productoController.getAllProducts)

routerfProductos.get('/:id', productoController.getProductById)

routerfProductos.post('/', productoController.postProduct)

routerfProductos.put('/:id', productoController.putProduct)

routerfProductos.delete('/:id', productoController.deleteProductById)

module.exports = routerfProductos