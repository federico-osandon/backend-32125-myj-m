const { CarritoDAOsMemoria } = require("../DAOs/CarritoDAOs/CarritoDAOsMemoria");

class CarritoController {

    constructor(){
        this.carritoDAOs = new CarritoDAOsMemoria()
    }



    getCarritoById =  async (req, res) => { 
        const { id } = req.query
        if (!id) {
            return res.status(401).json({msg: 'Se debe enviar un id'})
        }

        const carrito = await this.carritoDAOs.getById()
        if(carrito) return res.status(401).json({msg: 'No existe el carrito'})
        
        res.status(200).json(carrito)
    } 

}

module.exports = { CarritoController }